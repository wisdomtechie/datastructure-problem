package ds.graph;

import java.util.ArrayList;
import java.util.List;

public class CloudFrontCaching {

    public static void main(String[] args) {

        int[][] array = {
                {1, 2},
                {1, 3},
                {2, 4},
                {3, 5},
                {7, 8}
        };

        int count = 10;

        test(array, count);
    }


    public static void test(int[][] arr, int count) {
        List<List<Integer>> adjList = new ArrayList<List<Integer>>();


        for (int i = 0; i < count; i++) {
            adjList.add(new ArrayList<Integer>());
        }

        for (int[] edge : arr) {
            int edgeStart = edge[0];
            int edgeEnd = edge[1];
            adjList.get(edgeStart - 1).add(edgeEnd - 1);
            adjList.get(edgeEnd - 1).add(edgeStart - 1);
        }

        System.out.println("adjList = " + adjList);

        boolean[] visited = new boolean[count];

        int result = 0;
        List<Integer> results = new ArrayList<Integer>();

        for (int i = 0; i < count; i++) {
            if (visited[i] == false) {
                results.add(DFSRec(i, adjList, visited));
                result++;
            }
        }

        int cost = 0;
        for (int i = 0; i < results.size(); i++) {
            cost += Math.ceil(Math.sqrt(results.get(i)));
        }

        System.out.println("result = " + result);
        System.out.println("results = " + results);
        System.out.println("cost = " + cost);
    }

    public static int DFSRec(int src, List<List<Integer>> adjList, boolean[] visited) {
        visited[src] = true;
        int newCount = 1;
        for (int i = 0; i < adjList.get(src).size(); i++) {
            int vertex = adjList.get(src).get(i);
            if (visited[vertex] == false) {
                newCount += DFSRec(vertex, adjList, visited);
            }
        }
        return newCount;
    }


}
